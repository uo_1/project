# Reference : https://www.journaldev.com/40720/python-script-to-download-youtube-videos
# Source : Pytube Library : https://github.com/NFicano/pytube

from pytube import  YouTube

youtube_video_url = ''   # Put the URL for the video you wish to download

#def show_progress_bar(youtube_video_url, filters, bytes_remaining):
#    return  True  # do work

#YouTube.register_on_progress_callback(show_progress_bar)

try:
    yt_obj = YouTube(youtube_video_url)

    filters = yt_obj.streams.filter(progressive=True, file_extension='mp4')

    # download the highest quality video
    filters.get_highest_resolution().download(output_path='/home/studio/Videos',filename='')    # Filename here 
    # download the lowest quality video
    #filters.get_lowest_resolution().download(output_path='/home/studio/Videos',filename='')
    def show_progress_bar(youtube_video_url, filters, bytes_remaining):
        return show_progress_bar    # do work
    YouTube.register_on_progress_callback(show_progress_bar)

    print('Video Downloaded Successfully')
except Exception as e:
    print(e)

# Path to where to save video
#   download(output_path='/home/studio/Videos', filename='yt_video.mp4')
