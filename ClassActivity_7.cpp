/*
Created by : UniqueOne                          

                Introduction to Programming I
*/

// Question 7 :   Write a C program to accept four numbers and produce an output of their sum, average and product.

/*

#include<stdio.h>
int main()

{
    int num1,num2,num3,num4,result,average,product;
    printf("Enter the first number\n: ");
    scanf("%d",&num1);
    printf("Enter the second number\n: ");
    scanf("%d",&num2);
    printf("Enter the third number\n: ");
    scanf("%d",&num3);
    printf("Enter the fourth number\n: ");
    scanf("%d",&num4);

    result = num1 + num2 + num3 + num4;
    average = num1 + num2 + num3 + num4 /4;
    product = num1 * num2 * num3 * num4;
    printf("The  result of %d\n",result);
    printf("The result of %d\n",average);
    printf("The result of %d\n",product);
    return 0;

}

*/


/* Question 8 :

Write a C program to input the name of a DVD movie,
the date it was borrowed,
the rental fee charged and the amount of money paid in.
Compute the amount of money due back to the borrower and output it with a suitable label.

*/


#include<stdio.h>
#include<math.h>
int main()

{
    int result;
    int rental_fee,money_paid;
    char DVDname[30];
    char dateborrowed[30];

    printf("Enter the Name of DVD:\n ");
    scanf("%s",DVDname);
    printf("Enter the date borrowed:\n ");
    scanf("%s",dateborrowed);
    printf("Enter the money paid:$\n ");
    scanf("%d",&money_paid);
    printf("Enter the rental fee charged:$\n ");
    scanf("%d",&rental_fee);

    result = money_paid - rental_fee;
    printf("The total cost for DVD is $%d",result);
    //printf("The total cost for the DVD is $%d\n",result);                                   // Print the cost for the DVD
    //printf("The movie name was %s\n",DVDname);                                             // Print the Movie Name
    //printf("The date borrowed was %s\n",dateborrowed);                                    // Print the date borrowed
    //printf("The rental cost and the paid money was $%d, $%d\n ",rental_fee,money_paid);    // Print the rental cost and  money paid

    return 0;
}


/*

Question 9

Write a C program to prompt the user to input the ages of four of your friends.
The pseudocode should allow the user to input these ages,
find their average and print it with a suitable label.

*/


// START
// Declare age1,age2,age3,age4 , result as Integers
// Print "Enter the the first age"
// Read age1
// Print "Enter the the second age"
// Read age2
// Print "Enter the the third age"
// Read age3
// Print "Enter the the fourth age"
// Read age4
// result - age1 + age2 + age3 + age4 /4
// Print "The average age of the four friends is, "result
// STOP

/*

#include<stdio.h>
int main()

{
    int age1,age2,age3,age4,result;

    printf("Enter the first age of your friend: \n");
    scanf("%d",&age1);
    printf("Enter the second age of your friend: \n");
    scanf("%d",&age2);
    printf("Enter the third age of your friend: \n");
    scanf("%d",&age3);
    printf("Enter the fourth age of your friend: \n");
    scanf("%d",&age4);
    result = age1 + age2 + age3 + age4 /4;
    printf("The average age of four friends is %d",result);
    return 0;
}


*/
